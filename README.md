# Proyecto - Gajo

## web basada en react y firebase actualmente en desarrollo

### actualmente posee:

* un sistema de autenticacion firebase via google y de email

* un uso hooks de tipos context, useState, useEffect para la carga de marcadores en el mapa a travez de una consulta a firebase (dicha consulta esta gestionada en el backend por medio de firebase functions que al cargar o modificarse la informacion dada por el usuario actualizan una lista de puntos geograficos | esto podria hacerse con una api pero insumiria mayor cantidad de recursos que para un proyecto de esta envergadura no se justifica)

* uso de context, State, Effect para cambio automatico del estado de logueo del usuario

* un formulario gestionado desde un patron factory que permite el armado rapido de divesos inputs y el pasaje de funciones handleChance gestionadas por un estado para la recuperacion de informacion.
los inputs van desde el uso de un mapa, carga de imagenes y el uso de inputs mas basicos como textarea, select, radio y text

* un router que redirecciona al index y oculta rutas segun el estado automaticamente (si el user no esta logueado o no)

### a desarrollar:

* actualizar la pagina de carga para que tambien sirva de edicion

* firebase functions para modificar y eliminar la imagen en el storage y la geoPoint

* maquetado y animaciones

* pagina about y notFound

* cdn desde google Storage para la carga de imagenes dentro de los marcadores (o para el despliegue de un modal con img)
* aplicar seguridad por medio de secret keys (a investigar)
* fortalecer las rules de firebase
* un useMemo para que si el usuario entra a la pagina se cachee el mapa hasta que cargue/modifique un gajo o pase n° tiempo
* (falta agregar una gestion para sonido y video, a reutilizar en otros proyectos similares)
